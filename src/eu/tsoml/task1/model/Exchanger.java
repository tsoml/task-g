package eu.tsoml.task1.model;

public class Exchanger extends FinancialInstitution {

    private double usdLimit;

    public Exchanger(String name, double usdRate, double usdLimit) {
        super(name, usdRate);
        this.usdLimit = usdLimit;
    }


    public void convert(double amount) {
        if ((amount / getUsdRate()) <= usdLimit) {
            amount = amount / getUsdRate();
            System.out.println(getName() + ": " + "Your money is: " + amount + "USD");
        } else {
            System.out.println(getName() + ": " + "The exchanger has a limit of " + usdLimit);
        }
    }

}
