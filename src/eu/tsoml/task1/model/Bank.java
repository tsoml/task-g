package eu.tsoml.task1.model;

public class Bank extends FinancialInstitution {

    private double uahLimit;

    public Bank(String name, double usdRate, double uahLimit) {
        super(name, usdRate);
        this.uahLimit = uahLimit;
    }


    public void convert(double amount) {
        if (amount <= uahLimit) {
            amount = amount / getUsdRate();
            System.out.println(getName() + ": " + "Your money is: " + amount + "USD");
        } else {
            System.out.println("The bank has a limit of " + uahLimit);
        }
    }

}
