package eu.tsoml.task1.model;

public class FinancialInstitution {
    private String name;
    private double usdRate;

    public FinancialInstitution(String name, double usdRate) {
        this.name = name;
        this.usdRate = usdRate;
    }


    public String getName() {
        return name;
    }

    public double getUsdRate() {
        return usdRate;
    }


    public void convert(double amount){
        amount = amount/usdRate;
        System.out.println(getName() + ": " + "Your money is: " + amount + "USD");
    }

}
