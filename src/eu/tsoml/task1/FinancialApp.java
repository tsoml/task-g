package eu.tsoml.task1;

import eu.tsoml.task1.data.DataFactory;
import eu.tsoml.task1.model.Bank;
import eu.tsoml.task1.model.BlackMarket;
import eu.tsoml.task1.model.Exchanger;
import eu.tsoml.task1.model.FinancialInstitution;
import eu.tsoml.task2.data.Book;
import eu.tsoml.task2.data.Journal;
import eu.tsoml.task2.data.PrintedProduct;
import eu.tsoml.task2.data.Yearbook;

import java.util.Scanner;

public class FinancialApp {

    private FinancialInstitution[] financialInstitutions = DataFactory.generate();

    public void run() {

        Scanner scan = new Scanner(System.in);

        System.out.println("Task 1");
        System.out.print("Enter amount of UAH: ");
        double amount = Integer.parseInt(scan.nextLine());

        for (FinancialInstitution financialInstitution : financialInstitutions) {
            financialInstitution.convert(amount);
        }

        System.out.println("Task 2");
        System.out.print("Enter filter by (Bank, Exchanger or BlackMarket): ");
        String filter = scan.nextLine();

        switch (filter) {
            case "Bank":

                for (FinancialInstitution financialInstitution : financialInstitutions) {
                    if (financialInstitution instanceof Bank) {
                        financialInstitution.convert(amount);
                    }
                }
                break;

            case "Exchanger":

                for (FinancialInstitution financialInstitution : financialInstitutions) {
                    if (financialInstitution instanceof Exchanger) {
                        financialInstitution.convert(amount);
                    }
                }
                break;

            case "BlackMarket":
                for (FinancialInstitution financialInstitution : financialInstitutions) {
                    if (financialInstitution instanceof BlackMarket) {
                        financialInstitution.convert(amount);
                    }
                }
                break;
            default:
                System.out.println("NULL");
                break;
        }


    }
}
