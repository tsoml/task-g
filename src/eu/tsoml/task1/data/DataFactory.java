package eu.tsoml.task1.data;

import eu.tsoml.task1.model.Bank;
import eu.tsoml.task1.model.BlackMarket;
import eu.tsoml.task1.model.Exchanger;
import eu.tsoml.task1.model.FinancialInstitution;


public class DataFactory {

    public static final int PRODUCT_NUMBER = 3;

    public static FinancialInstitution[] generate() {

        FinancialInstitution[] financialInstitutions = new FinancialInstitution[PRODUCT_NUMBER];

        financialInstitutions[0] = new Bank("Monobank", 28, 150_000);
        financialInstitutions[1] = new Exchanger("TopTrader", 28.5, 20_000);
        financialInstitutions[2] = new BlackMarket("Jailbreak", 29);


        return financialInstitutions;
    }

}
