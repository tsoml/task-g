package eu.tsoml.task2.data;

public class Yearbook extends PrintedProduct {

    private String theme;
    private String publishingHouse;

    public Yearbook(String title, int yearOfPublication, String theme, String publishingHouse) {
        super(title, yearOfPublication);
        this.theme = theme;
        this.publishingHouse = publishingHouse;
    }

    public String getTheme() {
        return theme;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }


    @Override
    public String toString() {
        return String.format("%s; publishingHouse: %s; theme: %s", super.toString(), publishingHouse, theme);
    }

}
