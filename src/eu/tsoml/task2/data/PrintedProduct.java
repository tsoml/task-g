package eu.tsoml.task2.data;

public class PrintedProduct {

    private String title;
    private int yearOfPublication;

    public PrintedProduct(String title, int yearOfPublication) {
        this.title = title;
        this.yearOfPublication = yearOfPublication;
    }

    public String getTitle() {
        return title;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }


    @Override
    public String toString() {
        return String.format("Name: %s; year: %d ", title, yearOfPublication);
    }

}
