package eu.tsoml.task2.data;

public class Book extends PrintedProduct {

    private String author;
    private String publishingHouse;

    public Book(String title, int yearOfPublication, String author, String publishingHouse) {
        super(title, yearOfPublication);
        this.author = author;
        this.publishingHouse = publishingHouse;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }


    @Override
    public String toString() {
        return String.format("%s; author: %s; publishing house: %s", super.toString(), author, publishingHouse);
    }

}
