package eu.tsoml.task2.data;

public class Journal extends PrintedProduct {

    private String theme;
    private int monthOffPublication;

    public Journal(String title, int yearOfPublication, String theme, int monthOffPublication) {
        super(title, yearOfPublication);
        this.theme = theme;
        this.monthOffPublication = monthOffPublication;
    }

    public String getTheme() {
        return theme;
    }

    public int getMonthOffPublication() {
        return monthOffPublication;
    }


    @Override
    public String toString() {
        return String.format("%s; month: %s; theme: %s", super.toString(), monthOffPublication, theme);
    }

}
