package eu.tsoml.task2;

import eu.tsoml.task2.data.Book;
import eu.tsoml.task2.data.Journal;
import eu.tsoml.task2.data.Yearbook;
import eu.tsoml.task2.data.PrintedProduct;
import eu.tsoml.task2.model.ProductFactory;

import java.util.Scanner;

public class LibraryApp {

    private PrintedProduct[] products = ProductFactory.generate();

    public void run() {

        Scanner scan = new Scanner(System.in);

        System.out.println("Task 1");
        System.out.print("Enter year: ");
        int year = Integer.parseInt(scan.nextLine());

        for (PrintedProduct product : products) {
            if (product.getYearOfPublication() == year) {
                System.out.println(product.toString());
            }
        }

        System.out.println("Task 2");
        System.out.print("Enter filter by (Book, Journal or Yearbook): ");
        String filter = scan.nextLine();


        switch (filter) {
            case "Book":

                for (PrintedProduct product : products) {
                    if (product instanceof Book) {
                        System.out.println(product.toString());
                    }
                }
                break;

            case "Journal":

                for (PrintedProduct product : products) {
                    if (product instanceof Journal) {
                        System.out.println(product.toString());
                    }
                }
                break;

            case "Yearbook":
                for (PrintedProduct product : products) {
                    if (product instanceof Yearbook) {
                        System.out.println(product.toString());
                    }
                }
                break;
            default:
                System.out.println("NULL");
                break;
        }
    }
}
