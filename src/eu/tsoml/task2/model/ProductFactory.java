package eu.tsoml.task2.model;

import eu.tsoml.task2.data.Book;
import eu.tsoml.task2.data.Journal;
import eu.tsoml.task2.data.PrintedProduct;
import eu.tsoml.task2.data.Yearbook;

public class ProductFactory {

    public static final int PRODUCT_NUMBER = 6;

    public static PrintedProduct[] generate() {

        PrintedProduct[] products = new PrintedProduct[PRODUCT_NUMBER];

        products[0] = new Book("The Last Wish", 1993,"Andrzej Sapkowski", "Orbit");
        products[1] = new Book("Sword of Destiny",1992, "Andrzej Sapkowski", "Orbit");
        products[2] = new Journal("The 10X Rule", 2011, "Motivation", 11);
        products[3] = new Journal("Before the Storm", 2018, "Fantasy Fiction", 10);
        products[4] = new Yearbook("Good Omens", 2006, "Fantasy", "William Morrow" );
        products[5] = new Yearbook("Tales from the Shadowhunter Academy", 2016, "Novellas", "Simon & Schuster");

        return products;
    }

}
